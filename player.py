import dataset

db = dataset.connect('sqlite:///game.db')
players = db['Players']
inventories = db['Inventories']
locations = db['Locations']

class Player:
    def __init__(self, name, creation_date):
        self.name = name
        self.clan = ''
        self.last_login_date = creation_date
        self.creation_date = creation_date
        self.kill_count = 0
        self.password = ''
        self.player_id = 0
        self.item_id = 0
        self.use_count = 0
        self.type_storage = 0 # 1 - belt, 2 - backpack, 3 - storage
        self.location_id = 0
        self.last_visit_date = ''
        self.create_new_player()
    
    def get_old_information(self):
        try:
            data = players.find_one(id=self.player_id)
            self.name = data['name']
            self.clan = data['clan']
            self.creation_date = data['creation_date']
            self.last_login_date = data['last_login_date']
            self.password = data['password']
            self.kill_count = data['kill_count']
        except Exception as e:
            print(e)
        try:
            data = locations.find_one(player_id=self.player_id)
            self.location_id = data['location_id']
            self.last_visit_date = data['last_visit_date']
        except Exception as e:
            print(e)


    def create_new_player(self):
        finder = players.find_one(player_id=self.player_id)
        tmp_dict = dict(name=self.name, clan=self.clan, creation_date=self.creation_date, last_login_date=self.last_login_date, password=self.password, kill_count=self.kill_count)
        if finder:
            players.update(tmp_dict)
        else:
            players.insert(tmp_dict)
        self.player_id = (players.find_one(name=self.name)['id'])
        self.get_old_information()

    def storage_item(self):
        if self.type_storage in (1,2,3):
            finder = inventories.find_one(player_id=self.player_id, item_id=self.item_id)
            tmp_dict = dict(player_id=self.player_id, item_id=self.item_id, use_count=self.use_count, type_storage=self.type_storage)
            if finder:
                inventories.update(tmp_dict, ['player_id', 'item_id'])
            else:
                inventories.insert(tmp_dict)
    
    def change_location(self):
        finder = locations.find_one(player_id=self.player_id)
        tmp_dict = dict(player_id=self.player_id, location_id=self.location_id, last_visit_date=self.last_visit_date)
        if finder:
            inventories.update(tmp_dict, ['player_id'])
        else:
            locations.insert(tmp_dict)